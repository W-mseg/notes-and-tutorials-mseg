# Guides des bonnes pratiques discord.js

## Step one

avoir node est indispensable allez donc voir node.js, installez le avant de continuer ce guide.

installer la librairie discord.js dans son repo.

```shell
npm install discord.js
```

En prévision d'un probleme que vous trouverez très vite horrible c'est de devoir relancer votre serveur local a chaque modifications aussi petite soit elle.

Pour palier à ce problème nous allons installer nodemon qui va vous permettre de laisser le serveur ouvert et de tester en permanence vos changement sans fermer et relancer votre serveur, installez nodemon ainsi dans votre repo :

```shell
npm install nodemon
```

creez un index.js et importer discord.js de la manière suivante :

```javascript
const Discord = require("discord.js");
const bot = new Discord.Client();
```

ceci est une ancienne syntaxe mais fonctionne toujours pour ce genre d'utilisation

ensuite mettez y une autre ligne :

```javascript
const fs = require("fs");
```

cette ligne permet d'utiliser les fonctionalités systeme indispensable aux applications node.js.

afin de savoir si le bot s'est bien lancé nous faison une petite ligne de code en tant que bonne pratique :

```javascript
bot.on("ready", () => console.log(`${bot.user.username} est prêt à être utilisé`));:
```

ceci permet simplement d'afficher le nom du bot ainsi qu'un message indiquant qu'il s'est bel et bien lancé, dans le cas contraire le serveur ne démarerait meme pas.

maintenant il nous faut mettre une ligne indispensable au bon fonctionnement du bot

```javascript
bot.login(JSON.parse(fs.readFileSync("token.json")));
```

vous vous souvenez de votre fichier .json avec votre token ? c'est dedans que vous allez pouvoir le mettre a la place de "token.jon".

ceci va vous permettre de décoder votre token et donc de ne pas le mettre directement dans votre application mais dans un fichier apars.

ceci etant dis il vous faudra creer un fichier ".gitignore"

afin d'empecher d'envoyer certains fichiers dans github tel que les nodes modules ou encore votre token ! Ca serrait dramatique si quelqu'un prendrait le controle de votre bot et ferait n'importe quoi sur votre serveur !

dans ce fichier .gitignore ouvrez le et marquez simplement :
```json
node_modules
token.json
```

ainsi plus de souci votre token est sauvé et les node modules qui sont super lourd ne seront pas envoyés sur le serveur github , gain de sécurité et de vitesse !

maintenant pour creer vos commandes , on utilise souvent un préfixe , je vous conseille donc de creer une variable avec votre préfixe si au cas ou vous viendriez à le changer par la suite, vous n'aurez qu'une variable à changer et non toute les commandes une a une et oui cela peux arriver surtout lors de conflit avec d'autres bots !

une fois que vous avez créé votre variable avec votre préfixe, voyons comment creer votre première fonction.

```javascript
/**
creation de la variable "msg" pour la fonction
"message" qui est propre a la librairie discord.js
*/
bot.on('message',msg=>{
if (msg.content === prefixe+hello){
msg.reply('Hello World !');
}
}
```

Comme vous le comprenez msg.content récupere le contenu des messages sur le serveur et si le message est strictement égal au résultat voulu alors nous créons notre code à ce moment la !

Félicitation vous avez créé votre bot , votre première commande , vos bonnes pratiques maintenant il ne te reste qu'a comprendre la librairie discord.js et si tu veux vraiment faire un travail de pro , renseigne toi sur la programmation orienté objet avec javascript ;)

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/Markdown/Creer%20son%20bot%20discord.md">Previous </a>Creer son premier bot discord

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/README.md">vers le menu principal</a>