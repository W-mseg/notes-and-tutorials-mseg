# Creer son bot discord

Tout d'abord rendez vous à cette adresse pour la premiere étape, connectez vous afin d'y creer une nouvelle application : https://discord.com/developers/applications

Cliquez donc sur "New Application"

Votre bot est créé mais nous avons besoins de faire quelques configurations.

allez dans l'onglet "bot", cliquez sur "add bot" et cliquez sur "reveal token" au dessus qui est très important copiez le gardez le pour la suite.

Maintenant que vous avez sauvegardé votre token, creez un fichier .json dans lequel vous ne mettez que votre token, ceci est TRES important , cela empeche d'autres personnes malveillantes de hacker votre bot on le verra après pourquoi.

retournez sur votre application sur discord web et allez dans l'onglet Oauth2 vous avez une grille , cliquez sur "bot" et dans bot permission cliquez sur "administrator", maintenant copiez le lien Oauth généré et lancez le dans votre navigateur. maintenant que ce bot est intégré dans le serveur de votre choix il ne vous reste plus qu'a le coder , passez donc a la seconde étape.

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/Markdown/Guides%20des%20bonnes%20pratiques%20discord.js.md">Next </a>Les bonnes pratiques pour creer son bot

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/README.md">vers le menu principal</a>
