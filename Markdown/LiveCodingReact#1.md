# LiveCoding React#1

<a href="../README.md">vers le menu principal</a>

## Qu'est ce que React

---

* React est une librairie créée par facebook qui à pour but de faire des application web en one page,
son interêt se trouve justement dans le principe de creer des "single page web app".

* React est la partie VUE du modèle (MVC) et dont utilisable avec d'autre frameworks ou librairies (Comme AngularJS,laravel,symfony).

* React est utilisé par :

  * Netflix ( coté serveur pour gagner 50% de perf).
  * Facebook (of course)
  * Instagram
  * WhatsApp
  * mais aussi : Yahoo, Aribnb, Sony ...

## Ses avantages

---

* Peux être couplé à d'autres libs, frameworks, technologies.
* Une rapidité de mise en place des élements via les COMPONENTS.
* Une optimisation RADICALE des performances du site.
* Grosse popularité et donc grosse communauté.
* Travail très facile à trouver en tant que dev React.js .

## Ses principes de fonctionnement

---

* Utilisation de Components ( comme angular ou vue.js ).
* Une actualisation de components et non de page !
* Utilisations des "Render" et des "States".
* DOM Virtuel.

## Conventions

---

* Majuscule à chaque Components !
* JSX
* ES6
* SCSS pour chaque component individuellement avec node-sass.
* Utilisation d'une classe par élement et non d'une classe par comportement ex  

ceci :  

```Javascript
<div classname={'styleDIV'}>

<h1 classname={'styleH1'}>Texte</h1>

</div>
```

Plustot que cela :  
  
```html
<div classname={'inline-block'}>

<h1 classname={'fontsize'}>Texte</h1>

</div>
```  

### notions plus complexes

* Utilisation de constructors pour initialiser les states.
* Séparer les Component de LAYOUT et de UI
* Les Component ne doivent pas avoir d'attribut "style"
* Préférez les rendu conditionel complexe plustôt que les simples

## Comment creer un projet React.JS

---
<p align=center>
/!\ avec le terminal /!\
</p>

[Github de C.R.A](https://github.com/facebook/create-react-app)

avec npx

```npx
npx create-react-app nomdevotreapplication
```

avec npm

```npm
npm init react-app nomdevotreapplication
```

avec yarn

```yarn
yarn create react-app nomdevotreapplication
```

ensuite  

```terminal
cd nomdevotreapplication
```

```npm start```

## Creer un component

---

![image](../images/Component.png)

A mettre imprérativement dans le dossier "src" de react et creer le dossier "Components" vous même , les components sont dans un dossier comportant le nom voulu , dans ce dossier un fichier javascript du même nom (avec majuscule) et son fichier scss corespondant.

### Component stateless

---

![image](../images/stateless.png)

Premiere chose TOUJOURS importer la librairie dans le fichier javascript avec :  

```Javascript
import React from 'react';
```

ensuite creer une fonction flechée, tout les component sont ou sous forme de fonction flechée (pour les component stateless , en general dans la section UI).  
Ou sous forme de classe (en général, pour les component de type Layout et avec state).

```Javascript
const Example=()=>{

    return(

        <div> contenu de la div </div>

        );

    };
```

et enfin pour pouvoir utiliser le component partout dans notre application :  

```Javascript
export default Example;
```

### Component with state

---

![image](../images/withstate.png)

Les composant avec state sont la pour stocker des données supplémentaires mais pourquoi faire un state alors qu'on pourrait faire nos fonctions et variables à l'exterieur ?  
Parce que React.js charge TOUJOURS le state en premier, si il n'y à pas de state il chargera tout simplement component par component de manière classique , l'interêt se trouve encore une fois dans l'optimisation.

pour creer ce genre de component, il nous faut creer une classe oui mais il faut tout d'abord importer un props à react, le 'Component' !

Pour se faire tu dois ajouter

```javascript
,{Component}
```

APRES "import React" et avant "from 'react';"

ensuite seulement tu peux creer une classe mais avec "extends Component" derrière le nom de ta classe.  
Tu n'es pas obligé d'ajouter le state mais il est conseillé si tu as des infos a load en premier lieu.  

ensuite tu dois ajouter un render et ca c'est OBLIGATOIRE avec une classe étendue en component. Et seulement après tu peux y mettre ton "return".

l'export par défaut est toujours le même.

## importer un component

---

![image](../images/importcomponent.png)

Si tout c'est bien déroulé qu'il n'y à pas d'erreur il vous suffira d'appeller votre component par son nom et ce à l'interieur d'une balise orpheline ou non si vous voulez y incorporer des enfants de composants.

Mais surtout ne pas oublier d'importer votre component avec :  

```javascript
import Example from "../../lien/relatif/du/component";
```

## props

---

On est déja bien avancé mais nous nos composant ils sont statique et on aimerais bien ne pas avoir à creer 3 composant différent tout ca parce que nous avons troix titre différents et c'est la que se dévoile la puissance de react.

ceci est un component qu'on fait jusque maintenant :  
![image](../images/props1.png)

nous allons rendre notre titre modulaire grâce à un props !

Voici à quoi devras ressembler vos component stateless :
![image](../images/props2.png)

Et vos component with state :
![image](../images/props3.png)

Cependant on peux remarquer un problème si nous laissons un component orphelin tel quel (sans aucun props ni enfant)  
![image](../images/orphelin.png)  

cela nous donne ceci :  
![image](../images/semantique.png)

dans le meilleur des cas nous avons un problême de sémantique qui, sur des projets en large scale, sont problématique car retrouve UN component parmis des cnetaines voir des milliers, cela fait perdre énormément de temps.

Dans le pire, nous avons une erreur qui empêche tout bonement l'application de fonctionner.

Il nous faut donc changer la forme de nos props vers quelque chose de plus optimisé.

Comme cela:  
![image](../images/syntaxe.png)

Cette première méthode est la plus conseillée car en cas d'erreur, cela n'impacte en rien le reste de l'application.

Deuxième méthode moins conseillée mais viable :  
![image](../images/syntaxe2.png)  

Elle est toutefois déconseillée car elle oblige tout de même la sémantique et le remplissage de contenu forcé, ce qui ne convient pas à tout les types de components.

## les fonctions dans React

---

Pour creer une fonction il y à une PSR importante dans les nommations c'est d'ajouter "Handle" devant le nom de la fonction en plus de devoir être explicite tel que : "HandleClick", "HandleScroll" etc...  

![image](../images/functions.png)

Les fonctions ne peuvent s'écrire uniquement au dessus de la classe,
ensuite pour attributer la fonction à un élément il faut tout d'abord lui assigner un évenement ( ne peux etre assigné uniquement sur une balise html et non sur un component ) sous la forme de "on"+ l'evenement que vous souhaitez comme "onClick" , "onDrag", "onSubmit" etc... voir la documentation react pour voir les possibilités.

## le scss

---

C'est bien beau on sait tout creer mais pour styliser tout ca on va avoir besoins d'un atout : node-sass.  
Que l'on installe via terminal dans notre projet avec la commande :  

```terminal
npm install node-sass
```

c'est un outil qui va nous permettre d'intégrer nos scss directement en les important dans notre component en faisant un :  

```javascript
import './nomdevotrefichierscss';
```

aussi simple que ca, comme vu précédemment nous mettons une classe par element html dans nos component et nous gerons les classes individuellement.

### quid des images

---

de la meme manière qu'on importe un fichier scss dans un component
on importe une image (attention pour des raisons obscure il vaux mieux les importer dans le component plustot que dans le state)

via:  

```javascript
import nomdelimage from './liendevitreimage'
```

## chapitre final

---

### le déploiement

si votre application est finie il vous suffit de lancer dans le dossier de votre projet via le terminal :  

```terminal
npm run build
```

ce qui va creer une version optimisée de votre projet vous pouvez le deployer gratuitement sur netlify mais pour le deployer sur apache je vous laisse avec la vidéo de antho welc qui l'explique bien mieux que je ne l'expliquerait :  

<a href="https://www.youtube.com/embed/JBr3DIr3cIs" targer="blank">Video Antho Welc deployer sur apache</a>

et pour les hardus : <a href="https://gist.github.com/ywwwtseng/63c36ccb58a25a09f7096bbb602ac1de">Lien du github pour deployer sur apache</a>

.  
.  
.  

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/README.md">vers le menu principal</a>

By MarcoSegretario
