# Live Coding js

## chapter one

---

### Variables et scopes

Il y a 4 types de variables :

- Bool ( true or false)

- string ( chaine de charactere )

- int ( nombre rond : 1,2,5,10,13725)

- float ( nombre a décimale : 1.34,0.232 )

En javascript pas besoins de typer ses variable, c'est principalement ce qui fait que c'est un language de haut niveau, autrement dis du niveau facile ( haut niveau : éloigné du code machine et bas niveau : proche du code machine ).

Ce qui fait donc que nous n'avons que deux choses a retenir : let et const.

Pourquoi ces deux la au lieu de var ? Var est tout simplement obsolete, il est donc formellement interdit de l'utiliser et utiliser let et const.

chose a retenir au niveau des PSR :

- let s'écrit toujours avec des minuscule et camelcase (nomDeMaVariable)

- Const s'écrit toujours en majuscule et il est impossible de changer sa valeur (NOMDEMAVARIABLE)
Pourquoi on se permet d'ajouter const si c'est un let plus restreint c'est parce qu'il permet d'économiser des ressources et de ne consommer que ce qu'il a besoins dans un site web c'est UUULTRA important.

pour faire une variable globale avec un let il suffit de la mettre un cran au dessus de la fonction désirée c'est ce qu'on appelle le scope.

Une variable créée dans une boucle reste dans la boucle et le meme nom peux etre utilisé dans une autre boucle.

Creer une variable a l'interieur d'une fonction permet que la variable est accessible dans toute la fonction mais pas en dehors, c'est un concept très important à comprendre en tant que dev javascript.

## chapter two

---

### Les operations

en javascript 2+2 ne fait pas forcément 4

il faut savoir qu'il y a des choses absurdes dans javascript

surtout au niveau des opérateurs c'est pourquoi quand on fait une comparaison ( == )
il est plustot consseillé et en fait dans la pratique , obligatoire d'utiliser "==="

preuve en est :

```javascript
[1,2] == '1,2'
```

result true

```javascript
[1,2] === '1,2'
```

result false

il y a la concaténation dont il faut savoir c'est qu'on est pas obligé d'additionner un chiffre avec un chiffre mais aussi un chiffre avec une chaine de caractère ex : 'machintruc' + 6 = 'machintruc6'

je vous déconsseille les multiplications et divisions en dehors des operations mathématiques , vous ne trouverez jamais votre compte.

l'operateur xor '^' je ne l'ai jamais utilisé dans un cas concret sachez juste qu'il va calculer en fait selon les bits en binaire de la premiere variable et la deuxieme variable

### des petites astuces

---

Il y a tout de meme des petites astuces disponible principalement que dans javascript qui peuvent aider à rendre votre code plus lisible ou plus austère pour un débutant

```javascript
let variable = true;
variable?(+n**)**/4^'three':null
```

Asutère mais terriblement efficace

tout d'abord ** permet de creer une puissance 2 bien plus pratique que Math.pow(n,2)

+v un '+' devant une variable essayera toujours de convertir la variable en int, pratique dans beaucoup de cas et bien plus simple que tostring(variable)

? :

L'opération ternaire est un sacré morceau il permet de raccourcir grandement du code (if else)

'?' permet de vérifier si la variable est true ou false mais n'est pas applicable uniquement avec les booleans, il permet de définir si n'importe quelle variable est true, a savoir est ce que la variable existe, si oui il applique le code qui suit sinon (:) execute le code d'après

c'est une fonctionnalité ES6 très importante surtout quand vous irez dans les frameworks javascript très utile surtout dans reactJS ou ES6 fait partie des PSR

## Conditions

---

### if , else , else if , switch

if = si en anglais , else = sinon, else if = sinon si, switch = changement.

la syntaxe d'un if et d'un if else :

```javascript
if(argument de la condition){
    contenu de la condition
}
else if(argument de la condition){
    contenu de la condition
}
else
{
    contenu si aucune condition ne coresspond ca serrait comme un etat par defaut
}
```

pour le switch:

```javascript
switch(expression)
{
    case a:
    contenu de code;
    break;
    case b:
    contenu de code;
    break;
    case c:
    contenu de code;
    break;
    default:
    contenu de code;
    break;
}
```

Si l expression est égale à un des case , il execute le code du case avec un default qui permet comme son nom l'indique d'avoir un état par défaut,
son avantage c'est d'être plus propre et si il y a une verification très simple et repetitive à faire.

## les loops

il y a plusieurs type de boucles : les for , foreach , while.

les boucles peuvent toutes etre utilisées dans tout les cas mais on aura des préférences comme la boucle for pour parcourir des tableaux, la boucle foreach pour parcourir des objets et la boucle while pour que l'execution du code se fasse au moins une fois.

il y a de bien nombreux parametres mais pour les principales, à savoir for et foreach.

syntaxe de for :

```javascript
for (valeure initiale; condition; incrémentation ou decrementation){
    instruction qui se réitère
}
```

attention l'incrémentation est très importante afin de ne pas faire des boucles infinies , cependant c'est possible de le faire dans les cas ou nous désirons volontairement une boucle infinie

syntaxe de foreach :

```javascript
    object.forEach(element => fonction{
        contenu de la fonction
    });
```

syntaxe de while :

```javascript
while (condition)
{
    bout de code
}
```

penser à donner une limite a votre boucle avec incrémentation etc...

## les fonctions

les fonctions sont des morceau de code que l'on peux rappeller à plusieurs endroit, vous creez la portion de code une fois et vous pouvez la rappeller plusieurs fois, la fonction vient avec la base "dont repeat yourself".

syntaxe d'une focntion :

```javascript
function function_name(){
    portion de code;
}
```

ou anonyme

```javascript
function(){
    portion de code;
}
```

si elle est appellée directement mais nous pouvons nommer une fonction pour la rappeller et rendre le code plus joli.

Avec es6 on utilise la fonction flechée qui fonctionne comme un "return".

```javascript
const name =()=>console.log('Hello World');
```

mais peux aussi etre appellée avec des brackets :

```javascript
const name =()=>{
    portion de code;
}
```

n'est pas LA norme mais une norme ES6 qui est surtout la version utilisée pour reactJS.

---

## le DOM

le dom te permet de selectionner des elements HTML/css et d'interagir avec depuis le javascript.

tu peux ajouter du html depuis le javascript tout comme tu peux ajouter une classe css à ton element

comment ? grace à document.

```javascript
document.getElementById('nomid').textContent;
```

ce code permet de selectionner le texte contenu dans l'élément html avec l'id 'nomid'

imaginons que cette meme selection nous voulons remplacer ce texte, il suffira de faire :

```javascript
document.getElementByID('nomid').textContent = 'hello world';
```

et ainsi, a condition que notre html soit correct, cela changera le contenu acutel de la balise contenant l'id 'nomid' pour y afficher 'hello world'.

Regarder la doc afin de connaitre toutes les exclusivités du dom il faut retenir que le dom permet d'interagir avec le html/css de toute manière que ce soit.

## promise

```javascript

function faireQqc() {
    return new Promise((successCallback, failureCallback) => {
        console.log("C'est fait");
        // réussir une fois sur deux
        if (Math.random() > .5) {
            successCallback("Réussite");
        } else {
            failureCallback("Échec");
        }
    })
}

const promise = faireQqc();
promise.then(successCallback, failureCallback);
```

Il n'y à pas un milliard de chose a expliquer avec les promises , dans une fonction , vous retourner une Promise qui contient deux argument, le premier en cas de réussite, le deuxieme en cas d'échec, on l'utilise surtout dans le cas de fetching d'API quand on ne sait pas combien de temps ca peux prendre sans savoir meme si ca va fonctionner, face à toutes ces variables ont a créé la promise
la promise en elle meme est comme une fonction qui permet de récuperer et en cas de réussite, vous y mettez une fonction et une autre différente en cas d'echec.
Il ne faut pas se leurrer cela ne sert que dans le cas ou on récupère une API mais c'est important de le savoir !

## async await

```javascript
async function(){
    morceau de code
    deuxieme morceau
    await troixieme morceau
    quatrième morceau
    await cinquieme morceau
    sixieme morceau
}
```

Async await permet de rendre le code asynchrone et le rendre synchrone par moment

async = asynchrone
await = attend !

on définit que la fonction est asynchrone et il executera les différentes fonctions aussi tot que le thred sur lequel fonctionne la fonction est disponible autrement dis on peux dire c'est immédiat et donc calcule plusieurs choses en meme temps et await permet de dire à la fonction 'attend mon coco avant de continuer tu finit entierement ce thread la"

ce qui permet entre autre de faire des fonctions parfois complexe sans utiliser de promises et de setTimeOut qui parfois sont complexe a utiliser.

## try catch

TRES TRES TRES important qui permet de tester si une api ou quelque chose fonctionne avant d'être deployée.

```javascript
try{
    mon code qui est executé si il fonctionne, est envoyé
    throw {permet de gerer une exception}
}
catch{
    ce que le code renvoie si il ne fonctionne pas
}
```

c'est tout ce qu'il y à a savoir sur trycatch mais c'est vital lors du fetch d'API

---

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/README.md">vers le menu principal</a>
