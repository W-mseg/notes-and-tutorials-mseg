# LivesCodings and notes

Ceci est le repository fait pour le livecoding/workshop à destination des eleves de becode contenant les résumés des livecoding afin que tous puissent en profiter.

## react

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/Markdown/LiveCodingReact%231.md">Summary #1</a> La base complete pour react

Summary #2 Les notions avancées avec le state (A venir)

## javascript

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/Markdown/javascript.md">Summary #1</a> Les bases pour javascript

Summary #2 soon (programmation orienté objet)

## discord.js

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/Markdown/Creer%20son%20bot%20discord.md">Summary #1 </a>Creer son premier bot discord

<a href="https://gitlab.com/W-mseg/reactlivecoding-becode/-/blob/master/Markdown/Guides%20des%20bonnes%20pratiques%20discord.js.md">Summary #2 </a>Les bonnes pratiques pour creer son bot
